const Enterprise = require('../models/Enterprise');
var cont = 1;

module.exports = {

    // Listagem de empresas
    async index(req, res) {

        const reqName = req.query.name;
        const reqType = req.query.enterprise_types;

        // caso não venha parâmetros na query
        if (reqName == null && reqType == null) {
            const enterprises = await Enterprise.find().collation({ locale: "en" }).sort('name');
            return res.status(200).json(enterprises);
        }

        // caso venha String de busca e tipo de empresa na query
        if (reqName != null && reqType != null) {
            const enterprises = await Enterprise.find({ 'name': { $regex: '.*' + reqName + '.*', $options: 'i' }, 'enterprise_types': reqType });

            if (enterprises == "") {
                return res.json({ 'status': 204, 'message': 'No content' });
                //return res.status(204).json({ 'status': 204, 'message': 'No content' });
            } else {
                return res.status(200).json(enterprises);
            }
        }

        // caso venha apenas tipo de empresa na query
        if (reqName == null) {
            const enterprises = await Enterprise.find({ 'enterprise_types': reqType });

            if (enterprises == "") {
                return res.json({ 'status': 204, 'message': 'No content' });
                //return res.status(204).json({ 'status': 204, 'message': 'No content' });
            } else {
                return res.status(200).json(enterprises);
            }
        }

        // caso venha apenas String de busca na query
        if (reqType == null) {
            const enterprises = await Enterprise.find({ 'name': { $regex: '.*' + reqName + '.*', $options: 'i' } });

            if (enterprises == "") {
                return res.json({ 'status': 204, 'message': 'No content' });
                //return res.status(204).json({ 'status': 204, 'message': 'No content' });
            } else {
                return res.status(200).json(enterprises);
            }

        }


    },

    // Exibição de detalhes da empresa
    async show(req, res) {

            const idx = req.params.idx;
            const reqEnterprise = await Enterprise.find({ 'id': idx });

            // validar se existe empresa com id informado

            if (reqEnterprise == "") {
                return res.status(404).json({ 'status': 404, 'message': 'Resource not found' });
            } else {
                return res.status(200).json(reqEnterprise);
            }


    },

    //Método auxiliar para criar empresas
    async create(req, res) {
        const newName = req.body.name;
        const newType = req.body.enterprise_types;
        const newEnterprise = await Enterprise.create({ 'name': newName, 'enterprise_types': newType, 'id': cont++ });
        return res.status(201).json(newEnterprise);
    }

};
