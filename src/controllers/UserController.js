const User = require('../models/User');
const randtoken = require('rand-token');
const jwt = require('jsonwebtoken');
const config = require('../config');

module.exports = {
    async login(req, res) {

        // busca usuário e senha da requisição no banco de dados e retorna _id em reqUser

        User.find({ 'email': req.body.email, 'password': req.body.password }, '_id', function (err, reqUser) {
            if (reqUser == "") {
                return res.status(404).json({ 'status': 404, 'message': 'User or password incorrect' });
            } else {
                // geração do Json web token
                let token = jwt.sign({ email: req.body.email },
                    config.secret,
                    {
                        expiresIn: '30m'
                    }
                );
                
                // acess_token é o token jwt
                const access_token = token;
                // client é gerado aleatoriamente
                const client = randtoken.generate(16);
                // uid é o id do usuário no banco de dados
                const uid = reqUser[0]._id;

                res.setHeader('access-token', access_token);
                res.setHeader('client', client);
                res.setHeader('uid', uid);

                return res.status(200).json({ 'status': 200, 'message': 'Ok' });
            }
        }

        );

    },

    //Método auxiliar para criar usuários
    async create(req, res) {
        const newEmail = req.body.email;
        const newPassword = req.body.password;

        //Busca por usuário para determinar se já existe
        User.find({ 'email': req.body.email }, (err, reqUser) => {

            if (reqUser == "") {
                User.create({ 'email': newEmail, 'password': newPassword });
                return res.status(201).json({ 'status': 201, 'message': 'Created' });

            } else {
                return res.status(403).json({ 'status': 403, 'message': 'User already exists' });
            }
        })

    },

};


