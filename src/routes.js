const express = require('express');
const routes = express.Router();
const UserController = require ('./controllers/UserController');
const EnterpriseController = require ('./controllers/EnterpriseController');
const validate = require('./validateToken');

// rotas auxiliares utilizadas para preparar o banco de dados
//routes.post('/api/v1/users/auth/create', UserController.create);
//routes.post('/api/v1/enterprises', EnterpriseController.create);

routes.post('/api/v1/users/auth/sign_in', UserController.login);
routes.get('/api/v1/enterprises', validate.checkToken, EnterpriseController.index);
routes.get('/api/v1/enterprises/:idx', validate.checkToken, EnterpriseController.show);

module.exports=routes;