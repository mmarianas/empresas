const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const app = express();
mongoose.connect('mongodb+srv://mariana:1234@cluster0-lbue2.mongodb.net/test?retryWrites=true&w=majority',{useNewUrlParser:true});

app.use(bodyParser.json());
app.use(require('./routes'));

app.listen(8080);


    