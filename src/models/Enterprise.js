const mongoose = require('mongoose');

const EnterpriseSchema = new mongoose.Schema({
    name: { type: String, required: true, index: { unique: true } },
    enterprise_types: { type: Number, required: true },
    id: { type: Number, required: true }
}
);

module.exports = mongoose.model('Enterprise', EnterpriseSchema);